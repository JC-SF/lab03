package main;
import LinearAlgebra.Vector3d;
public class Vector {
	public static void main(String[] args) {
		Vector3d vec = new Vector3d(12.5,13,-5);
		Vector3d vec1 = new Vector3d(-2,-6.5,7.6);
		System.out.println(vec.getX()+" "+vec.getY()+" "+vec.getZ());
		System.out.println(vec1.getX()+" "+vec1.getY()+" "+vec1.getZ());
		System.out.println(vec.magnitude());
		System.out.println(vec.dotProduct(vec1));
		
	}
}
