package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testVectGetMethods() {
		Vector3d vec = new Vector3d(-1.23,67.8,-90);
		assertEquals(-1.23,vec.getX());
		assertEquals(67.8,vec.getY());
		assertEquals(-90,vec.getZ());
	}
	@Test
	void testVectMagnitude() {
		Vector3d vec = new Vector3d(12,-4.678,-0.09);
		// Expected result from Math.sqrt(/*12*12+4.678*4.678+0.09*0.09) 
		assertEquals(12.8798984468,vec.magnitude(), 0.000001);
	}
	@Test
	void testVectDotProduct() {
		Vector3d vec = new Vector3d(12,-4.678,-0.09);
		Vector3d vec1 = new Vector3d(-1.23,67.8,-90);
		// Expected result from 12*-1.23+-4.678*67.8+-0.09*-90
		assertEquals(-323.8284 ,vec.dotProduct(vec1),0.0001);
	}
	@Test
	void testVectAdd() {
		Vector3d vec = new Vector3d(12,-4.678,-0.09);
		Vector3d vec1 = new Vector3d(-1.23,67.8,-90);
		Vector3d methodAddedVec = vec.add(vec1);
		// Expected result from 12-1.23 
		assertEquals(10.77, methodAddedVec.getX(), 0.00000001);
		// Expected result from -4.678+67.8
		assertEquals(63.122, methodAddedVec.getY(), 0.00000001);
		// Expected result from -0.09-90
		assertEquals(-90.09, methodAddedVec.getZ(), 0.00000001);
		
		
	}
}
