package LinearAlgebra;

public class Vector3d {
	private double x,y,z;
	public Vector3d(double coordx, double coordy, double coordz) {
		x = coordx;
		y = coordy;
		z = coordz;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getZ() {
		return z;
	}
	public double magnitude() {
		return Math.sqrt(x*x+y*y+z*z);
	}
	public double dotProduct(Vector3d myVect) {
		return this.x*myVect.x + this.y*myVect.y + this.z*myVect.z;
	}
	public Vector3d add(Vector3d vec1) {
		return new Vector3d(this.x + vec1.x, this.y + vec1.y, this.z + vec1.z);
		
	}
}
